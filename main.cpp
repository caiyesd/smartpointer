#include <stdio.h>
#include <stdlib.h>

#include <utils/RefBase.h>
#include <utils/StrongPointer.h>

using namespace android;

class T : public RefBase {
public:
    T() { printf("T created %p\n", this); }
    virtual ~T() { printf("T destroyed %p\n", this); }
    void fn() { printf("T fn() called %p\n", this); }
//private:
    int mData;
};

sp<T> test() {
    sp<T> p1 = new T();
    sp<T> p2 = new T();
    return p1;
}

int main(int argc, char *argv[]) {
    sp<T> p = new T();
    p->fn(); // == p.get()->fn(); "p->" == "p.m_ptr->"
    (*p).mData = 1; //"*p" == "*(p.m_ptr)"
    
    /*{
        sp<T> p1 = test();
        wp<T> p3 = p1;
        sp<T> p4 = p3.promote();
        p4->fn();
        p1 = NULL;
    }*/
    system("PAUSE");
    return EXIT_SUCCESS;
}
